#!/usr/bin/env python

from dateutil import parser as date_parser
from shutil import rmtree
from trending.corpus_builder import build_corpus, to_whoosh_index

import delegator
import newspaper as news
import parsel
import urllib.parse
import sys, pathlib, glob, json, functools, os, re, uuid
import urllib.parse
import util as h

NORM_CMD = delegator.run('which norm').out.strip()

boilerplate = {
    'video': re.compile('The opinions expressed on this video.+this website or call on 0370 270 6000\.', flags=re.DOTALL),
    'login': re.compile('your account has been suspended.+how to change your password\.', flags=re.DOTALL)
}

class Posts(object):
    """
    Posts is a generic class that help us to extend to every necessary class
    for the different companies.

    """

    def clean_boilerplate(self, article):
        for k, v in boilerplate.items():
            article.set_text(re.sub(v, '\n', article.text))
        return article

    @functools.lru_cache(maxsize=10)
    def create_selector(self, html):
        return parsel.Selector(text=html)

    def __init__(self, name=""):
        """Init with the name that indentify the company website"""
        self.name = name

    def get_name(self):
        return self.name

    def read_normalised_text(self, file_path):
        try:
            cmd = '{} "{}"'.format(NORM_CMD, file_path)
            c = delegator.run(cmd)
            try:
                return c.out.encode("utf8").decode('utf8')
            except Exception as ex:
                h.print_err('error in encoding/decoding UTF8: {}'.format(ex))
                return c.out
        except Exception as e:
            h.print_err('error in normalising: {} -> {}'.format(file_path, e))
            return h.read_from_file(file_path)

    def to_json(self, article):
        keywords = article.meta_keywords + list(article.tags)
        kv = {
            'url': article.canonical_link,
            'keywords': keywords,
            'title': article.title,
            'description': article.meta_description,
            'text': article.text,
            'authors': article.authors,
            'publish_date': article.publish_date
        }
        return json.dumps(kv, indent=4)

    def transform_posts(self, file_paths):
        urls = [pathlib.Path(file_path).as_uri() for file_path in file_paths]
        for url in urls:
            profile = news.Article(url=url)
            full_url, file_path = '', ''
            try:
                if url.startswith('file://'):
                    full_url = urllib.parse.unquote(url)
                    file_path = urllib.parse.urlparse(full_url).path
                    html = self.read_normalised_text(file_path)
                    profile.download(input_html=html)
                else:
                    profile.download()
                profile.parse()
                profile.nlp()
                author, _ = self.find_authors(profile.html)
                if author:
                    profile.set_authors([author])
                if not profile.publish_date:
                    profile.publish_date = self.find_publish_date(profile.html)
                else:
                    if not isinstance(profile.publish_date, str):
                        profile.publish_date = profile.publish_date.isoformat()
                yield profile
            except news.ArticleException as e:
                h.print_err(
                    'error in processing article: {}, {}\nfull url: {}\nfile path: {}'.format(url, e, full_url, file_path))

class PostsBrowneJacobson(Posts):
    def __init__(self, name=""):
        self.name = name

    def find_authors(self, html):
        sel = self.create_selector(html)
        profile_link = sel.css('.profile.profile-leadcontact-right a.name-leadcontact').xpath('@href').extract_first()
        author_name = sel.css('.profile.profile-leadcontact-right a.name-leadcontact::text').extract_first()
        return author_name, profile_link

    def find_publish_date(self, html):
        sel = self.create_selector(html)
        text = sel.css('div.title.date::text').extract_first(default='1 January 1970').strip()
        return date_parser.parse(text).isoformat()

class PostsDwf(Posts):
    def __init__(self, name=""):
        self.name = name

    def find_authors(self, html):
        sel = self.create_selector(html)
        profile_link = sel.css('div.people.-custom a').xpath('@href').extract_first()
        author_name = sel.css('div.people.-custom h3.people__item__name::text').extract_first()
        return author_name, profile_link

    def find_publish_date(self, html):
        sel = self.create_selector(html)
        text = sel.css('div.title.date::text').extract_first(default='1 January 1970').strip()
        return date_parser.parse(text).isoformat()

class PostsBrodies(Posts):
    def __init__(self, name=""):
        self.name = name

    def find_authors(self, html):
        sel = self.create_selector(html)
        profile_link = sel.css('.postAuthor a').xpath('@href').extract_first()
        author_name = sel.css('.postAuthor a::text').extract_first()
        return author_name, profile_link

    def find_publish_date(self, html):
        sel = self.create_selector(html)
        text = sel.css('.postExtendedDate>p::text').extract_first(default='1 January 1970').strip().replace('On ', '')
        return date_parser.parse(text).isoformat()

class PostsGowlingWLG(Posts):
    def __init__(self, name="", db="data.db"):
        self.name = name
        self.database = db

    def find_authors(self, html):
        sel = self.create_selector(html)
        profile_link = sel.css('a.print-author').xpath('@href').extract_first()
        author_name = sel.css('a.print-author h3::text').extract_first()
        return author_name, profile_link

    def find_publish_date(self, html):
        sel = self.create_selector(html)
        text = sel.css('div.insight-pub-date::text').extract_first(default='1 January 1970').strip()
        return date_parser.parse(text).isoformat()

def create_instance(site):
    if site == 'brownejacobson':
        instance = PostsBrowneJacobson(name=site)
    elif site.lower() == 'brodies':
        instance = PostsBrodies(name=site)
    elif site.lower() == 'gowlingwlg':
        instance = PostsGowlingWLG(name=site)
    else:
        print("The site name that you have provided is not correct.\nTry with one of this options: brownejacobson, brodies, gowlingwlg")
        sys.exit(0)
    return instance

def main(argv):
    import argparse
    parser = argparse.ArgumentParser(
        description='find the profile and the information for all the people in Browne Jacobson')
    parser.add_argument("-s", "--site", help='name of the web site. Options: brownejacobson, brodies, gowlingwlg')
    parser.add_argument("-f", "--folder", help='a top folder that contains all the profile pages')
    parser.add_argument("-o", "--output", help='output folder to put all the generated json files')
    parser.add_argument("--corpus", help='location of the corpus')
    parser.add_argument("--index", help='location of the index')
    parser.add_argument("input_file", type=str, help="the input files", nargs="*")
    args = parser.parse_args()
    input_files = []
    # create new instance for the posts of one company
    if args.site:
        instance = create_instance(args.site)
        out_folder = args.output + "/" + args.site
        if os.path.exists(out_folder):
            rmtree(out_folder)
        os.makedirs(out_folder)
        args.output = out_folder
    if args.input_file:
        if args.input_file == '-':
            input_files = [line.strip() for line in sys.stdin]
        else:
            input_files = args.input_file
    if args.folder:
        input_files = glob.glob(args.folder + "/**/*", recursive=True)
        input_files = [f for f in input_files if os.path.isfile(f)]

    if out_folder:
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
        for post in instance.transform_posts(input_files):
            file_name = str(uuid.uuid4()) + '.json'
            file_path = os.path.join(out_folder, file_name)
            with open(file_path, 'w') as f:
                f.write(instance.to_json(instance.clean_boilerplate(post)))
        json_files = glob.glob(out_folder + "/**/*", recursive=True)
        json_files = [f for f in json_files if os.path.isfile(f)]

        if args.corpus:
            corpus = build_corpus(json_files, output_location=args.corpus)
        # ToDo calculate phrases and add them to the docs in corpus
        if args.index:
            # The index name is given by de company plus a suffix _docs
            index_name = instance.get_name()+'_docs'
            to_whoosh_index(corpus, args.index, index_name=index_name)
            # adding all the new posts of this site into the general index
            to_whoosh_index(corpus, args.index, index_name='docs')

    else:
        for post in instance.transform_posts(input_files):
            print(instance.to_json(instance.clean_boilerplate(post)))


if __name__ == "__main__":
    main(sys.argv[1:])
