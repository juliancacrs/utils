#!/usr/bin/env python

import sys, glob, json, os.path, uuid, datetime
import dateutil.parser, dateutil.relativedelta as relative_date
import textacy
import nltk.corpus
import whoosh.index as w_index
import whoosh.fields as w_fields
import whoosh.qparser as w_q_parser
import util as h

LANG = 'en'
STOP_WORDS = set(nltk.corpus.stopwords.words('english'))

def read_json_files(input_files):
    for input_file in input_files:
        with open(input_file, 'r') as f:
            try:
                data = json.load(f)
                yield data
            except:
                h.print_err('ignoring a non-json file {}'.format(input_file))

def to_docs(json_data):
    for item in json_data:
        metadata = dict(item)
        del metadata['text']
        content = '\n'.join([item['title'], item['description'], item['text']])
        doc = textacy.Doc(content=content, metadata=metadata, lang=LANG)
        yield doc

def build_corpus(input_files, output_location=None):
    json_data = read_json_files(input_files)
    corpus = textacy.Corpus(lang=LANG, docs=to_docs(json_data))
    if output_location:
        h.make_dirs(output_location)
        corpus.save(output_location)
    return corpus

def looks_like_important_term(term):
    if term.lower() in STOP_WORDS:
        return False
    if ' ' in term:
        return True
    if '£' in term:
        return False
    if term.islower():
        return False
    return True

def filtered_terms_list(doc):
    def clean_phrase(text):
        return text.strip().lower().replace('\n', ' ')
    tags = set(e.text.lower() for e in textacy.extract.named_entities(doc, exclude_types='NUMERIC'))
    tags.update(clean_phrase(noun.text) for noun in textacy.extract.noun_chunks(doc) if looks_like_important_term(noun.text))
    # print('NOUNS -> {}'.format(', '.join([clean_phrase(noun.text) for noun in textacy.extract.noun_chunks(doc)])))
    # print('NOUNS-filtered -> {}'.format(
    #     ', '.join([clean_phrase(noun.text) for noun in textacy.extract.noun_chunks(doc) if looks_like_important_term(noun.text)])))
    tags.update(k.lower() for k in doc.metadata['keywords'] if k)
    terms_list = doc.to_terms_list(ngrams=(1, 2, 3), named_entities=True, exclude_types='NUMERIC', normalize='lower',
                                   as_strings=True)
    return [t for t in terms_list if t in tags]

def add_doc(writer, corpus):
    for doc in corpus.docs:
        doc.metadata['publish_date'] = dateutil.parser.parse(str(doc.metadata['publish_date']))
        doc.metadata['terms_list'] = filtered_terms_list(doc)
        writer.add_document(id=str(uuid.uuid4()), text=doc.text, **doc.metadata)

def get_schema():
    schema = w_fields.Schema(
        id=w_fields.ID(stored=True, unique=True),
        url=w_fields.STORED,
        authors=w_fields.KEYWORD(stored=True, commas=True),
        publish_date=w_fields.DATETIME(stored=True, sortable=True),
        keywords=w_fields.KEYWORD(stored=True, lowercase=True, commas=True),
        description=w_fields.TEXT(stored=True),
        title=w_fields.TEXT(stored=True),
        text=w_fields.TEXT,
        terms_list=w_fields.STORED
    )
    return schema

def to_whoosh_index(corpus, index_location, index_name='_docs'):
    if not os.path.exists(index_location):
        os.mkdir(index_location)
    if not w_index.exists_in(index_location, indexname=index_name):
        ix = w_index.create_in(index_location, schema=get_schema(), indexname=index_name)
    else:
        ix = w_index.open_dir(index_location, indexname=index_name)
    writer = ix.writer(procs=4, multisegment=True)

    add_doc(writer, corpus)

    writer.commit(optimize=True)

    print("Index for= "+ index_name +" have ="+str(ix.doc_count())+" items")
    ix.close()

def test_index(ix):
    query_parser = w_q_parser.QueryParser("text", schema=ix.schema)
    with ix.searcher() as searcher:
        q_texts = ['London', 'london', 'Browne Jacobson']
        for q_text in q_texts:
            q = query_parser.parse(q_text)
            results = searcher.search(q)
            print('query {} -> {}'.format(q_text, len(results)))
        for i, doc in enumerate(searcher.documents()):
            print('{}: {}'.format(i, doc['publish_date']))
            print(100 * '-')
        print('doc count: {}'.format(searcher.doc_count()))
        today = datetime.datetime.now()
        a_month_ago = today - relative_date.relativedelta(months=+1)
        a_month_in_2015 = dateutil.parser.parse('1 December 2015')
        query = 'publish_date:[{} to]'.format(a_month_in_2015.strftime('%Y%m%d'))
        print('query: {}'.format(query))
        results = searcher.search(q=query_parser.parse(query))
        print('results count: {}'.format(len(results)))
        for i, doc in enumerate(results):
            print('{}: {}'.format(i, doc['publish_date']))
            print(100 * '*')


def main(argv):
    import argparse

    parser = argparse.ArgumentParser(
        description='find the profile and the information for all the people in Browne Jacobson')
    parser.add_argument("-s", "--server", help='the server address that serves the data')
    parser.add_argument("-f", "--folder", help='a top folder that contains all the profile pages')
    parser.add_argument("--corpus", help='location of the corpus')
    parser.add_argument("--index", help='location of the index')
    parser.add_argument("--db", help='location of the database')
    parser.add_argument("input_file", type=str, help="the input files", nargs="*")
    args = parser.parse_args()
    input_files = []
    if args.input_file:
        if args.input_file == '-':
            input_files = [line.strip() for line in sys.stdin]
        else:
            input_files = args.input_file
    if args.folder:
        input_files = glob.glob(args.folder + "/**/*", recursive=True)
        input_files = [f for f in input_files if os.path.isfile(f)]

    corpus = build_corpus(input_files, output_location=args.corpus)
    # ToDo calculate phrases and add them to the docs in corpus
    if args.index:
        to_whoosh_index(corpus, args.index, index_name='brownejacobson_docs')
        ix = w_index.open_dir(args.index, indexname='docs')
        test_index(ix)
        ix.close()


if __name__ == "__main__":
    main(sys.argv[1:])
